# Filmes

Este projeto tem como finalidade resolver o teste técnico elaborado pela OTO CRM. É de importância que o projeto possua os seguintes objetivos resolvidos:

- Usuário pode ver os filmes mais recentes na tela inicial
- Usuário pode clicar em qualquer filme e ver uma página dedicada do filme
- Usuário pode marcar um filme como favorito
- Usuário pode ver sua lista de filmes favoritos em uma página separada

Foi utilizada a [API da TMDB](https://developers.themoviedb.org/3), e para que seja possível testar o projeto, você irá precisa da API_KEY. Você consegue pegar realizando o login na sua conta do[THEMOVIEDB](https://www.themoviedb.org/) e acessando a página [API](https://www.themoviedb.org/settings/api). Copie sua API e adicione na composable useApi.ts

O projeto possui a organização da arquitetura ITCSS para organizar os arquivos SCSS, e também a metodologia BEM para melhorar a legibilidade e produtividade.

### Instale as dependências

```
yarn install
```

### Inicie o servidor de desenvolvimento

```
yarn serve
```

## TO DO

Abaixo consta uma lista de melhorias que é interessante melhorar no projeto, e algo importante para softwares reais:

- Melhorar a tratativa de erros
- Implementar testes de usabilidade, performance e acessibilidade
- Padronizar identidade visual
- Migrar webpack para o vite, isso soluciona problemas encontrados de incompatibilidade utilizando dotenv.
- Padronizar e expandir uso de variáveis globais
