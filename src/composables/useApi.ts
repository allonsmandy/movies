import axios from "axios";
import { reactive } from "vue";

const API_KEY = "c13b4e8212e238838a749328f0d42982";
const BASE_URL = "https://api.themoviedb.org/3";
const SESSION_ID = localStorage.getItem("session_id");

interface Params {
  api_key: string;
  session_id: string;
}

type FN = (value: any) => void;

const params = {
  api_key: API_KEY,
  session_id: SESSION_ID,
};

const state = reactive({
  data: [],
  favoriteMovie: {},
});

const get = async (
  url: string,
  params: Params | {},
  fnThen?: FN,
  fnCatch?: FN
) => {
  await axios
    .get(url, { params })
    .then((response) => {
      if (fnThen) fnThen(response);
      state.data = response.data;
    })
    .catch((error) => {
      if (fnCatch) fnCatch(error);
      console.log(error);
    });

  return state.data;
};

const post = async (url: string, data: any, fnThen?: FN, fnCatch?: FN) => {
  axios
    .post(url, data, {
      headers: { "Content-Type": "application/json;charset=utf-8" },
    })
    .then((response) => {
      state.data = response.data;
      if (fnThen) fnThen(response);
    })
    .catch((error) => {
      console.log(error);
      if (fnCatch) fnCatch(error);
    });
};

export function useApi() {
  const login = async () => {
    get(
      `${BASE_URL}/authentication/token/new?api_key=${API_KEY}`,
      {},
      (response) => {
        if (typeof response.data == "string") {
          response.data = JSON.parse(response.data);
        }

        const TOKEN = response.data.request_token;

        window.location.href = `https://www.themoviedb.org/authenticate/${TOKEN}?redirect_to=${location.protocol}//${location.host}/profile`;
      }
    );
  };

  const getNowPlaying = async (page = 1) => {
    return await get(`${BASE_URL}/movie/now_playing?api_key=${API_KEY}`, {
      language: "pt-BR",
      page,
    });
  };

  const getFavorites = async () => {
    return await get(`${BASE_URL}/account/{account_id}/favorite/movies`, {
      ...params,
      language: "pt-BR",
      sort_by: "created_at.asc",
      page: 1,
    });
  };

  const getMovie = async (id: number) => {
    const movie = await get(`${BASE_URL}/movie/${id}?api_key=${API_KEY}`, {
      language: "pt-BR",
    });

    const isFavorite: any = await get(
      `${BASE_URL}/movie/${id}/account_states`,
      { ...params },
      (response) => {
        state.favoriteMovie = response.data.favorite;
        return;
      }
    );

    return {
      ...movie,
      favorite: isFavorite.favorite,
    };
  };

  const hasFavorite = async (id: number) => {
    await get(
      `${BASE_URL}/movie/${id}/account_states`,
      { ...params },
      (response) => {
        state.favoriteMovie = response.data.favorite;
        return;
      }
    );
  };

  const addFavorite = async (id: number, status: boolean) => {
    console.log("status", status);
    return await post(
      `${BASE_URL}/account/15248864/favorite?api_key=${API_KEY}&session_id=${SESSION_ID}`,
      { media_type: "movie", media_id: id, favorite: status }
    );
  };

  const getUserInfo = async (session_id: string) => {
    return await get(
      `${BASE_URL}/account?api_key=${API_KEY}&session_id=${session_id}`,
      {},
      (response) => {
        localStorage.setItem("user", JSON.stringify(response.data));
        localStorage.setItem("account_id", response.data.id);
        window.location.href = "/";
      },
      () => {
        // store.commit("logOut")
      }
    );
  };

  const createSession = async (token: string) => {
    return await post(
      `${BASE_URL}/authentication/session/new?api_key=${API_KEY}&request_token=${token}`,
      {},
      (response) => {
        if (response.data.success) {
          localStorage.setItem("session_id", response.data.session_id);
          getUserInfo(response.data.session_id);
        }
      }
    );
  };

  return {
    login,
    getNowPlaying,
    getFavorites,
    getMovie,
    addFavorite,
    createSession,
    getUserInfo,
    hasFavorite,
  };
}
