import { createApp } from "vue";
import App from "./App.vue";
import store from "./store";
import router from "./router";

const app = createApp(App);

app.config.globalProperties.$filters = {
  formatDate(date: string) {
    return Intl.DateTimeFormat("pt-BR", {
      year: "numeric",
      month: "long",
      day: "numeric",
    }).format(new Date(date));
  },
  formatRuntime(valueInMinutes: number) {
    const hours = Math.floor(valueInMinutes / 60);
    const minutes = Math.round(hours * 60);
    return hours + "h " + minutes + "m";
  },
};

app.use(router).use(store).mount("#app");
