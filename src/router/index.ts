import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import HomeView from "../views/now-playing.vue";
import FavoritesMovies from "../views/favorites-movies.vue";
import MoviePage from "../views/movie-page.vue";
import ProfilePage from "../views/my-profile.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "home",
    component: HomeView,
    meta: { title: "Mais recentes" },
  },
  {
    path: "/now-playing",
    name: "now-playing",
    component: HomeView,
    meta: { title: "Mais recentes" },
  },
  {
    path: "/movie/:id",
    name: "movie",
    component: MoviePage,
    props: true,
    meta: { title: "Filme" },
  },
  {
    path: "/profile",
    name: "profile",
    component: ProfilePage,
    props: true,
    meta: { title: "Meu perfil" },
  },
  {
    path: "/favorites",
    name: "favorites",
    component: FavoritesMovies,
    props: true,
    meta: { title: "Filmes favoritos" },
  },
];

const router = createRouter({
  history: createWebHistory("/"),
  routes,
  strict: true,
});

router.beforeEach((to, from, next) => {
  if (to.meta.title) {
    /* @ts-ignore */
    document.title = to.meta.title;
  }
  next();
});

export default router;
