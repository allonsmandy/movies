import { createStore } from "vuex";
import { useRouter } from "vue-router";

const router = useRouter();

export default createStore({
  getters: {
    sessionId: () => {
      return localStorage.getItem("session_id");
    },
    accountId: () => {
      return localStorage.getItem("account_id");
    },
    getUser: () => {
      const hasUser = localStorage.getItem("user");
      return hasUser ? JSON.parse(hasUser) : null;
    },
  },
  mutations: {
    logOut() {
      localStorage.clear();
      window.location.href = "/";
    },
  },
});
